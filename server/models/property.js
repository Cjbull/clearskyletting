const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const propertySchema = new Schema({
  address: String,
  footage: String,
  userId: String
});

module.exports = mongoose.model('Book', propertySchema);
