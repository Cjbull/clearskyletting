const graphql = require('graphql');
const _ = require('lodash');
const Property = require('../models/property');

const {GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull
} = graphql;

const PropertyType = new GraphQLObjectType({
  name: 'Property',
  fields: () => ({
    id: {type: GraphQLID},
    address: {type: GraphQLString},
    footage: {type: GraphQLString},
    user: {
      type: UserType,
      resolve(parent, args){
        return User.findById(parent.userId);
      }
    }
  })
});

const UserType = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: {type: GraphQLID},
    name: {type: GraphQLString},
    properties: {
      type: new GraphQLList(PropertyType),
      resolve(parent, args){
        return Property.find({ userId: parent.id});
      }
    }
  })
});

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    property: {
      type: PropertyType,
      args: {id: {type: GraphQLID}},
      resolve(parent, args){
        return Property.findById(args.id);
      }
    },
    user: {
      type: UserType,
      args: {id: {type: GraphQLID}},
      resolve(parent, args){
        return User.findById(args.id);
      }
    },
    properties: {
      type: new GraphQLList(PropertyType),
      resolve(parent, args){
        return Property.find({});
      }
    },
    users: {
      type: new GraphQLList(UserType),
      resolve(parent, args){
        return User.find({});
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addUser: {
      type: UserType,
      args: {
        name: {type: new GraphQLNonNull(GraphQLString)},
      },
      resolve(parent, args){
        let user = new User({
          name: args.name
        });
        return user.save();
      }
    },
    addProperty: {
      type: PropertyType,
      args: {
        address: {type: new GraphQLNonNull(GraphQLString)},
        footage: {type: new GraphQLNonNull(GraphQLString)},
        userId: {type: new GraphQLNonNull(GraphQLString)}
      },
      resolve(parent, args){
        let property = new Property({
          address: args.address,
          footage: args.footage,
          userId: args.userId
        });
        return property.save();
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
