import React, {Component} from 'react';
import {graphql} from 'react-apollo';
import {getPropertiesQuery} from '../queries/queries';
import PropertyDetails from './PropertyDetails';

class PropertyList extends Component{
  constructor(props){
    super(props);
    this.state = {
      selected: null
    }
  }
  displayProperties(){
    var data = this.props.data;
    if(data.loading){
      return(<div>Loading Properties</div>);
    }else{
      return data.properties.map(property =>{
        return(
          <li key={property.id} onClick={(e) => {this.setState({selected: property.id})}}>{property.address}</li>
        );
      })
    }
  }
  render(){
    return (
      <div>
        <ul id="property-list">
          {this.displayProperties()}
        </ul>
        <PropertyDetails propertyId={this.state.selected}/>
      </div>
    );
  }
}

export default graphql(getPropertiesQuery)(PropertyList);
