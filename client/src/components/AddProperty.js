import React, {Component} from 'react';
import {graphql} from 'react-apollo';
import {getUsersQuery, addPropertyMutation, getPropertiesQuery} from '../queries/queries';
import {flowRight as compose} from 'lodash';

class AddProperty extends Component{
  constructor(props){
    super(props);
    this.state={
      address: "",
      footage: "",
      userId: ""
    };
  }
  displayUsers(){
    var data = this.props.getUsersQuery;
    if(data.loading){
      return(<option>Loading User</option>);
    }else{
      return data.users.map(user => {
        return(<option key={user.id} value={user.id}>{user.name}</option>);
      })
    }
  }
  submitForm(e){
    e.preventDefault();
    this.props.addPropertyMutation({
      variables: {
        address: this.state.address,
        footage: this.state.footage,
        userId: this.state.userId
      },
      refetchQueries: [{query: getPropertiesQuery}]
    });
  }
  render(){
    return (
      <form id="add-property" onSubmit={this.submitForm.bind(this)}>
      <h2>Add to favourites:</h2>
        <div className="field">
          <label>Property Address: </label>
          <select name= "Property" type="text">
            <option onSelect={(e) => this.setState({address:e.target.value})}></option>
          </select>
        </div>
      </form>
    );
  }
}

export default compose(
  graphql(getUsersQuery, {name: "getUsersQuery"}),
  graphql(addPropertyMutation, {name: "addPropertyMutation"})
)(AddProperty);
