import React, {Component} from 'react';
import {graphql} from 'react-apollo';
import {getPropertyQuery} from '../queries/queries';

class PropertyDetails extends Component{
  displayPropertyDetails(){
    const{property} = this.props.data;
    if(property){
      return(
        <div>
          <h2>{property.address}</h2>
          <p>{property.footage}</p>
        </div>
      )
    }else{
      return(
        <div>no property selected</div>
      )
    }
  }
  render(){
    return (
      <div id="property-details">
        {this.displayPropertyDetails()}
      </div>
    );
  }
}

export default graphql(getPropertyQuery, {
  options: (props) => {
    return{
      variables:{
        id: props.propertyId
      }
    }
  }
})(PropertyDetails);
