import {gql} from 'apollo-boost';

const getPropertiesQuery = gql`
  {
    properties{
      address
      id
    }
  }
`

const getUsersQuery = gql`
  {
    users{
      name
      id
    }
  }
`

const addPropertyMutation = gql`
  mutation($address: String!, $footage: String!, $userId: ID!){
    addProperty(address: $address, footage: $footage, userId: $userId){
      address
      id
    }
  }
`

const getPropertyQuery = gql`
  query($id: ID){
    property(id: $id){
      id
      address
      footage
      user{
        id
        name
        properties{
          address
          id
        }
      }
    }
  }
`

export {getUsersQuery, getPropertiesQuery, addPropertyMutation, getPropertyQuery};
