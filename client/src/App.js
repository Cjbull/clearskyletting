import React, {Component} from 'react';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';
import PropertyList from './components/PropertyList';
import AddProperty from './components/AddProperty';

const client = new ApolloClient({
  uri:'http://localhost:4000/graphql'
});

class App extends Component{
  render(){
    return (
      <ApolloProvider client={client}>
        <div id="main">
          <h1>ClearSky Letting</h1>
            <div id="property-list">
              <PropertyList/>
            </div>
            <div id="add-property">
              <AddProperty/>
            </div>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
